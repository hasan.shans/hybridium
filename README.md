Project has two parts:


1) DbUpdater - for updating database. Just upload DbUpdater.exe file to server and schedule it (i.e by Task Scheduler) for any time interval to automatize updating process.

2) Hybridium API - fetchs location data by any given IP address. example: localhost/api/Geo?ip=12.19.50.0/23


Swagger endpoint: /swagger/v1/swagger.json